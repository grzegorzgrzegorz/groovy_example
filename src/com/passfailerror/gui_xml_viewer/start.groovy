package com.passfailerror.gui_xml_viewer

import groovy.swing.SwingBuilder
import groovy.util.slurpersupport.GPathResult

import javax.swing.event.DocumentListener
import java.awt.BorderLayout


def xml

new SwingBuilder().edt {
    frame(title: 'XML explorer', size: [1000, 500], show: true) {
        borderLayout()


        def buildNodePath = { String nodePath ->
            def node = xml
            nodePath.split("\\.").each { node = node."${it}" }
            return node
        }

        def quickList = { String nodePath ->
            def result = []

            def node = buildNodePath(nodePath)


            if (element.text == null) {
                println "checking" + it.@"${attribute.text}"
                result.add(it.@"${attribute.text}")

            } else {
                node.each {
                    println "checking " + it.@"${attribute.text}"
                    if ((it.@"${attribute.text}" as String).startsWith(nameLookup.text)) {
                        result.add(it.@"${attribute.text}")
                    }
                }
            }
            println "result: ${result}"
            return result as String[]
        }

        def autoComplete = {
            if (xml == null) {
                xml = new XmlSlurper().parse(new File(path.text))
                current_file.text = path.text
            }
            combo.removeAllItems()
            quickList(element.text).each { combo.addItem(it) }
        }


        def getAttributeSummaryOfElement = { String nodePath, String attributeValue ->
            def node = buildNodePath(nodePath)
            def result = []
            node.each {
                if (it.@"${attribute.text}" == attributeValue) {
                    println "value is: ${it.text()}"
                    println "attributes are:"
                    it.attributes().each { k, v -> println "${k}:${v}" }
                    result.add(it)
                }
            }
            return result
        }

        panel(constraints: BorderLayout.NORTH,
                border: compoundBorder([emptyBorder(10), titledBorder('Enter path:')])) {
            tableLayout {
                tr {
                    td { label 'File to open (ENTER to load new file):' }
                    td { textField(id: 'path', columns: 50, text = "C:\\Program Files (x86)\\Notepad++\\config.model.xml", actionPerformed: {
                        xml = new XmlSlurper().parse(new File(path.text))
                        current_file.text = path.text
                    }) }
                }
                tr {
                    td { label 'currently loaded file: ' }
                    td { label(id: 'current_file') }
                }


            }
        }

        panel(constraints: BorderLayout.CENTER,
                border: compoundBorder([emptyBorder(10), titledBorder('Enter element name:')])) {
            tableLayout {
                tr {
                    td { label 'Element to show:' }
                    td { textField(id: 'element', columns: 50, text = "GUIConfigs.GUIConfig")
                        // NotepadPlus.GUIConfigs.GUIConfig
                    }
                }
                tr {
                    td { label 'Attribute to use:' }
                    td { textField(id: 'attribute', columns: 50, text = "name") }
                }
                tr {
                    td { label "Start typing attribute value to filter dropdown or ENTER to load all:" }
                    td { textField(id: 'nameLookup', columns: 20, actionPerformed: { autoComplete() })
                        nameLookup.document.addDocumentListener(
                                [insertUpdate : {
                                    autoComplete()
                                },
                                 removeUpdate : {
                                     autoComplete()
                                 },
                                 changedUpdate: {
                                     autoComplete()
                                 }] as DocumentListener)
                    }
                }
                tr {
                    td { comboBox(id: 'combo', actionPerformed: {
                        def result = getAttributeSummaryOfElement(element.text, combo.getSelectedItem())
                            result.each {
                                attribute_summary.text = "<html>value is: ${it.text()} <br> attributes are: ${it.attributes()}</html>"
                            }
                        })
                    }
                    td { label(id: 'attribute_summary') }
                }


            }
        }


    }
}
