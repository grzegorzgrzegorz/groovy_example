package com.passfailerror.dir_browser

class Entity {

    static files = "files"
    static directories = "directories"
    static XML = "xml"
    static TXT = "txt"
    static EXE = "exe"
    static DLL = "dll"
    static EMPTY = "empty"

    static Thing with(Map args){
        return new Thing("with", args)
    }


}
