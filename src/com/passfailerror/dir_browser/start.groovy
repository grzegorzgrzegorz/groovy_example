package com.passfailerror.dir_browser


import static com.passfailerror.dir_browser.Actions.browse
import static com.passfailerror.dir_browser.Entity.*

browse directory: "C:\\Program Files (x86)\\Notepad++", for: directories
browse directory: "C:\\Program Files (x86)\\Notepad++", for: files
browse directory: "C:\\Program Files (x86)\\Notepad++", for: files, with (extension: XML)
browse directory: "C:\\Program Files (x86)\\Notepad++", for: files, with (extension: DLL)
browse directory: "C:\\Program Files (x86)\\Notepad++", for: files, with (extension: EMPTY)