package com.passfailerror.dir_browser

import groovy.io.FileType

class Actions {

static browse(Map args, Thing thing=new Thing("empty")){

    def directoryContent = new File("${args.directory}")

    switch ("${args.for}"){
        case "files":
            println "files"
            if (!thing.getName().contentEquals("empty")) {
                def extension = thing.getParam().extension as String
                println "(using extension: ${extension})"
                if (extension.contentEquals("empty")){
                    directoryContent.traverse(type: FileType.FILES, maxDepth: 0, nameFilter: ~/[a-zA-Z0-9+]+(\.[a-zA-Z0-9+]{4,}$)*/) {
                        println it
                    }
                }
                else {
                    directoryContent.traverse(type: FileType.FILES, maxDepth: 0, nameFilter: ~/.*\.${extension}$/) {
                        println it
                    }
                }
            }
            else{
                directoryContent.traverse(type: FileType.FILES, maxDepth: 0){println it}
            }
            break
        case "directories":
            if (!thing.getName().contentEquals("empty")){
                println "you cannot have extension when listing directories: extension ignored"
            }
            println "directories"
            directoryContent.traverse(type: FileType.DIRECTORIES, maxDepth: 0){println it}
            break
        default:
            println "no such option"
    }


}

}
